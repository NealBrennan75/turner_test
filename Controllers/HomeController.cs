﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TurnerTest_MVC4_01112014.Models;
using System.Web.Script.Serialization;

namespace TurnerTest_MVC4_01112014.Controllers
{
    public class HomeController : Controller
    {

        private Titles objTitlesData = new Titles();
        
        
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            if(Request.QueryString["titleQuery"] != null){

                List<Title> listTitles = objTitlesData.List_Titles(Request.QueryString["titleQuery"]);
                return View("Results", listTitles);

            }
            else{
                return View();
            }


        }

        public ActionResult Detail(int intTitleID)
        {
            Title objTitle = objTitlesData.Title_Detail(intTitleID);
            return View("TitleDetail", objTitle);
        }

        public string listGenres(int intTitleID)
        {

            List<TitleGenre> listTitleGenres = objTitlesData.List_TitleGenres(intTitleID);

            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(listTitleGenres);

            return json;
        }


        public string listParticipants(int intTitleID)
        {

            List<TitleParticipant> listTitleParticipants = objTitlesData.List_TitleParticipants(intTitleID);

            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(listTitleParticipants);

            return json;
        }


        public string listAwards(int intTitleID)
        {

            List<TitleAward> listTitleAwards = objTitlesData.List_TitleAwards(intTitleID);

            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(listTitleAwards);

            return json;
        }


        public string listStorylines(int intTitleID)
        {

            List<TitleStoryline> listTitleStorylines = objTitlesData.List_TitleStorylines(intTitleID);

            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(listTitleStorylines);

            return json;
        }
    
    }
}
