﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;




namespace TurnerTest_MVC4_01112014.Models
{
    public class Title
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string TitleNameSortable { get; set; }
        public int TitleTypeId { get; set; }
        public int ReleaseYear { get; set; }
    }



    public class TitleGenre
    {
        public int TitleId { get; set; }
        public string Name { get; set; }
    }

    public class TitleParticipant
    {
        public int TitleId { get; set; }
        public string Name { get; set; }
        public string RoleType { get; set; }
    }


    public class TitleAward
    {
        public int TitleId { get; set; }
        public string Award { get; set; }
        public string AwardCompany { get; set; }
        public int AwardYear { get; set; }
    }



    public class TitleStoryline
    {
        public int TitleId { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }
    
    
    public class Titles
    {

        //Block for Creating Connection
        //#################################################################
        private string connStringTitles = ConfigurationManager.ConnectionStrings["connectionTurnerTest"].ConnectionString;
        //#################################################################  



        public List<Title> List_Titles(string strQuery)
        {

            SqlDataReader reader;
            var listTitles = new List<Title>();

            using (SqlConnection conn = new SqlConnection(connStringTitles))
            {

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Title WHERE TitleName LIKE '%" + strQuery + "%'", conn))
                {
                    conn.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listTitles.Add(new Title { TitleId = reader.GetInt32(0), TitleName = reader[1] as string ?? default(string), TitleNameSortable = reader[2] as string ?? default(string), TitleTypeId = reader[3] as int? ?? default(int), ReleaseYear = reader[4] as int? ?? default(int) });
                    }
                }

            }


            return listTitles;
        }


        public Title Title_Detail(int id)
        {

            SqlDataReader reader;
            var objTitleRecord = new Title();

            using (SqlConnection conn = new SqlConnection(connStringTitles))
            {

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Title WHERE TitleId = " + id.ToString(), conn))
                {
                    conn.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        objTitleRecord.TitleId = reader.GetInt32(0);
                        objTitleRecord.TitleName = reader[1] as string ?? default(string);
                        objTitleRecord.TitleNameSortable = reader[2] as string ?? default(string);
                        objTitleRecord.TitleTypeId = reader[3] as int? ?? default(int);
                        objTitleRecord.ReleaseYear = reader[4] as int? ?? default(int);
                    }
                }

            }


            return objTitleRecord;
        }




        public List<TitleGenre> List_TitleGenres(int intTitleID)
        {

            SqlDataReader reader;
            var listTitleGenres = new List<TitleGenre>();

            using (SqlConnection conn = new SqlConnection(connStringTitles))
            {

                using (SqlCommand cmd = new SqlCommand("SELECT TG.TitleID, GG.Name FROM TitleGenre TG INNER JOIN Genre GG ON TG.GenreID = GG.ID WHERE TG.TitleID = " + intTitleID, conn))
                {
                    conn.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listTitleGenres.Add(new TitleGenre { TitleId = reader.GetInt32(0), Name = reader[1].ToString()});
                    }
                }

            }


            return listTitleGenres;
        }





        public List<TitleParticipant> List_TitleParticipants(int intTitleID)
        {

            SqlDataReader reader;
            var listTitleParticipants = new List<TitleParticipant>();

            using (SqlConnection conn = new SqlConnection(connStringTitles))
            {

                using (SqlCommand cmd = new SqlCommand("SELECT TP.TitleID, PP.Name, TP.RoleType FROM TitleParticipant TP INNER JOIN Participant PP ON TP.ParticipantID = PP.ID WHERE isKey = 1 AND TP.TitleID = " + intTitleID + " ORDER BY RoleType DESC, Name ASC", conn))
                {
                    conn.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listTitleParticipants.Add(new TitleParticipant { TitleId = reader.GetInt32(0), Name = reader[1].ToString(), RoleType = reader[2].ToString() });
                    }
                }

            }


            return listTitleParticipants;
        }





        public List<TitleAward> List_TitleAwards(int intTitleID)
        {

            SqlDataReader reader;
            var listTitleAwards = new List<TitleAward>();

            using (SqlConnection conn = new SqlConnection(connStringTitles))
            {

                using (SqlCommand cmd = new SqlCommand("SELECT TitleId, Award, AwardCompany, AwardYear FROM Award WHERE titleID = " + intTitleID + " and awardWon = 1 order by AwardYear ASC, awardCompany ASC, Award ASC", conn))
                {
                    conn.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listTitleAwards.Add(new TitleAward { TitleId = reader.GetInt32(0), Award = reader[1].ToString(), AwardCompany = reader[2].ToString(), AwardYear = reader.GetInt32(3) });
                    }
                }

            }


            return listTitleAwards;
        }





        public List<TitleStoryline> List_TitleStorylines(int intTitleID)
        {

            SqlDataReader reader;
            var listTitleStorylines = new List<TitleStoryline>();

            using (SqlConnection conn = new SqlConnection(connStringTitles))
            {

                using (SqlCommand cmd = new SqlCommand("SELECT TitleId, Type, Description from storyLine where titleID = " + intTitleID , conn))
                {
                    conn.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        listTitleStorylines.Add(new TitleStoryline { TitleId = reader.GetInt32(0), Type = reader[1].ToString(), Description = reader[2].ToString()});
                    }
                }

            }


            return listTitleStorylines;
        }
    
    
    }





}