function detailScript(intTitleId) {

    console.log('detail script has been fired!');


    //GET GENRES
    //########################################################
    $.ajax({
        url: '/Home/listGenres?intTitleID=' + intTitleId,

        success: function (data) {
            var strGenres = '';

            $.each(data, function (index, item) {

                strGenres = strGenres + data[index].Name;

                if (index + 1 != data.length) {
                    strGenres = strGenres + ', ';
                };
            });

            $(".Genres").html(strGenres);

        },
        contentType: 'application/json',
        dataType: 'json'
    });
    //########################################################




    //GET PARTICIPANTS
    //########################################################
    $.ajax({
        url: '/Home/listParticipants?intTitleID=' + intTitleId,

        success: function (data) {

            if (data.length > 0) {
                $(".Participants").html("");
            }

            $.each(data, function (index, item) {

                $(".Participants").append(data[index].Name + ' (' + data[index].RoleType + ')<br/>');

            });


        },
        contentType: 'application/json',
        dataType: 'json'
    });
    //########################################################



    //GET AWARDS
    //########################################################
    $.ajax({
        url: '/Home/listAwards?intTitleID=' + intTitleId,

        success: function (data) {

            if (data.length > 0) {
                $(".Awards").html("");
            }

            $.each(data, function (index, item) {

                $(".Awards").append(data[index].AwardCompany + ': ' + data[index].Award + ', ' + data[index].AwardYear + '<br/>');

            });


        },
        contentType: 'application/json',
        dataType: 'json'
    });
    //########################################################



    //GET STORYLINES
    //########################################################
    $.ajax({
        url: '/Home/listStorylines?intTitleID=' + intTitleId,

        success: function (data) {

            if (data.length > 0) {
                $(".Storylines").html("");
            }

            $.each(data, function (index, item) {

                $(".Storylines").append('<em>[from ' + data[index].Type + ']</em> -- ' + data[index].Description + '<br/><br/>');

            });


        },
        contentType: 'application/json',
        dataType: 'json'
    });
    //########################################################




}